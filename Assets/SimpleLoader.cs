using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;

public class SimpleLoader : MonoBehaviour
{
    [SerializeField] AssetReference someAsset;
    [SerializeField] float downloadAmount;

    [SerializeField] List<string> storedLocation;
    AsyncOperationHandle currentOperation;

    AsyncOperationHandle<IList<IResourceLocation>> handle = new AsyncOperationHandle<IList<IResourceLocation>>();
    

    private void Start()
    {
        //Addressables.LoadResourceLocationsAsync("indo_buildings").Completed+= DoneLoadResourcesLocation;
        Addressables.LoadResourceLocationsAsync("INDO").Completed += (locs) =>
         {
             Debug.Log(locs.Result.Count);
             foreach (var obj in locs.Result)
             {
                 if (obj.ResourceType.Equals(typeof(GameObject)))
                 {
                     Debug.Log("Prim Key : " + obj.PrimaryKey);
                     Addressables.LoadAssetAsync<GameObject>(obj).Completed += LoadRemoteAsset;
                 }
             }
         };
    }

    void LoadMap()
    {
       // Addressables.InstantiateAsync()
    }
    private void LoadRemoteAsset(AsyncOperationHandle<GameObject> obj)
    {
        Debug.Log("Obj 1 :" + obj.Result.name);
    }
}
